
module SimpleForm
  module Components
    # Needs to be enabled in order to do automatic lookups
    module Metainstructions
      def metainstruction(wrapper_options = nil)    
        @metainstruction ||= begin
          metainstruction = options[:metainstruction]
          if metainstruction.is_a?(String)
            html_escape(metainstruction)
          else
            content = translate_from_namespace(:metainstructions)
            content.html_safe if content
          end
        end
      end

      # Used when the metainstruction is optional
      def has_metainstruction?
        options[:metainstruction] != false && metainstruction.present?
      end

      def metainstruction_button(wrapper_options = nil)
        if !has_metainstruction?
          return nil
        end
        id = options[:what] || ''
        button = content_tag(:button, '?', id: "#{id}-button",
                             class: 'btn btn-primary', type: 'button',
                             'data-toggle': 'collapse',
                             'data-target': "\##{id}-well", 
                             'aria-controls': "#{id}-well",
                             'aria-collapsed': 'false'
                            )


      end

      def metainstruction_well(wrapper_options = nil)
        if !has_metainstruction?
          return nil
        end
        id = options[:what] || ''
        well = content_tag(:div,  metainstruction(wrapper_options), 
                            class: 'well'
                          )
        container = content_tag(:div, well, class: 'collapse', id: "#{id}-well")

      end

    end
  end
end

SimpleForm::Inputs::Base.send(:include, SimpleForm::Components::Metainstructions)
